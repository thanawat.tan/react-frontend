// For Create Actions Type
export const createActionSet = actionName => ({
    PENDING: `${actionName}_PENDING`,
    SUCCESS: `${actionName}_SUCCESS`,
    FAILED: `${actionName}_FAILED`
})
