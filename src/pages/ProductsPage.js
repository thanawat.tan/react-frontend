// Libs
import React from 'react'

// Components 
import ProductList from '../components/ProductList'

const ProductPage = props => {
    return <ProductList />
}

export default ProductPage