// Libs
import React from 'react'

// Components
import HomeProductList from '../components/HomeProductList'

const HomePage = () => {

    return (
        <React.Fragment>
            <HomeProductList />
        </React.Fragment>
    )
}

export default HomePage

