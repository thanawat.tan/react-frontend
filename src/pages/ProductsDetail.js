// Libs
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

// Actions
import { findById } from '../actions/products'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
    mainFeaturedPost: {
        position: 'relative',
        backgroundColor: theme.palette.grey[800],
        color: theme.palette.common.white,
        marginBottom: theme.spacing(4),
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
    },
    overlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        backgroundColor: 'rgba(0,0,0,.3)',
    },
    mainFeaturedPostContent: {
        position: 'relative',
        padding: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(6),
            paddingRight: 0
        },
    }
}))

const ProductDetail = props => {

    const classes = useStyles();

    const { match } = props

    const dispatch = useDispatch()

    const products = useSelector(state => state.products.productsDetail.data)

    useEffect(() => {

        const { id } = match.params

        dispatch(findById(id))

    }, [dispatch, match])

    return (
        <div className='album py-5'>
            <div className='container'>
                <Paper className={classes.mainFeaturedPostContent} style={{ backgroundImage: `url(${products.products_image})` }} >
                    <img style={{ display: 'none' }} src={products.products_image} alt={'tedt'} />
                    <div className={classes.overlay} />
                    <Grid container>
                        <Grid item md={12}>
                            <div className={classes.mainFeaturedPostContent}>
                                <Typography component='h1' variant='h1' style={{ color: '#FFFFFF', fontWeight: 'bold' }}>
                                    {products.products_name}
                                </Typography>
                                <br />
                                <Typography component='h1' variant='h3' style={{ fontWeight: 'bold' }} color='secondary' gutterBottom>
                                    PRICE : {products.products_price}
                                </Typography>
                                <div style={{ marginLeft: '50px', marginTop: '250px' }}>
                                    <Typography component="h1" variant="h3" style={{ color: '#FFFFFF', fontWeight: 'bold' }} gutterBottom>
                                        {products.products_category}
                                    </Typography>
                                    <Typography component="h1" variant="h3" style={{ color: '#FFFFFF', fontWeight: 'bold' }} gutterBottom>
                                        {products.products_quantity}
                                    </Typography>
                                    <Typography component="h1" variant="h3" style={{ color: '#FFFFFF', fontWeight: 'bold' }} gutterBottom>
                                        {products.products_description}
                                    </Typography>
                                </div>

                            </div>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        </div >
    )
}

export default ProductDetail