// Utils
import { createActionSet } from '../utilities/index'
import { auth } from '../utilities/firebase'

// Libs
import { push } from 'connected-react-router'
import jwt from 'jsonwebtoken'

// Set Type Action For Authentication
export const USER_LOGIN = createActionSet('USER_LOGIN')
export const USER_REGISTER = createActionSet('USER_REGISTER')
export const USER_LOGOUT = createActionSet('USER_LOGOUT')
export const FETCH_CURRENT_USER = createActionSet('FETCH_CURRENT_USER')

const SECRET = 'MY_SECRET_KEY'

export const login = (email, password) => async dispatch => {

    dispatch({
        type: USER_LOGIN.PENDING
    })

    try {

        const res = await auth.signInWithEmailAndPassword(email, password)

        const { user } = res

        const currentUser = {
            email: user.email,
            name: user.displayName
        }

        dispatch({
            type: USER_LOGIN.SUCCESS,
            payload: currentUser
        })

        const dateNow = new Date()

        const payload = {
            ...currentUser,
            iat: dateNow.getTime() // มาจากคำว่า issued at time (สร้างเมื่อ)
        }

        const token = jwt.sign(payload, SECRET)

        localStorage.setItem('currentUser', token)

        dispatch(push('/'))

    }
    catch (error) {

        dispatch({
            type: USER_LOGIN.FAILED,
            error
        })

    }
}

export const register = (email, password) => async dispatch => {

    dispatch({
        type: USER_REGISTER.PENDING
    })

    try {

        const currentUser = await auth.createUserWithEmailAndPassword(
            email,
            password
        )

        dispatch({
            type: USER_REGISTER.SUCCESS,
            payload: currentUser
        })

        const dateNow = new Date()

        const payload = {
            ...currentUser,
            iat: dateNow.getTime() // มาจากคำว่า issued at time (สร้างเมื่อ)
        }

        const token = jwt.sign(payload, SECRET)

        localStorage.setItem('currentUser', token)

        dispatch(push('/'))

    }
    catch (error) {

        dispatch({
            type: USER_REGISTER.FAILED,
            error
        })

    }
}

export const logout = () => async dispatch => {

    dispatch({
        type: USER_LOGOUT.PENDING
    })

    try {

        await auth.signOut()

        dispatch({
            type: USER_LOGOUT.SUCCESS
        })

        localStorage.removeItem('currentUser')

        dispatch(push('/'))

    } catch (error) {

        dispatch({
            type: USER_LOGOUT.FAILED,
            error
        })

    }
}

export const fetchCurrentUser = () => dispatch => {

    dispatch({
        type: FETCH_CURRENT_USER.PENDING
    })

    auth.onAuthStateChanged(user => {

        if (user) {

            const currentUser = {
                email: user.email,
                name: user.displayName
            }

            dispatch({
                type: FETCH_CURRENT_USER.SUCCESS,
                payload: currentUser
            })

            const token = localStorage.getItem('currentUser')

            localStorage.setItem('currentUser', token)

        } else {

            dispatch({
                type: FETCH_CURRENT_USER.FAILED
            })

        }

    })

}
