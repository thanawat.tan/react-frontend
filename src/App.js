// Libs
import React from 'react'
import { withRouter } from 'react-router'
// Routing
import AppWithRouting from './routes'

// Components
import AppHeader from './components/AppHeader'

const App = () => {

  // const dispatch = useDispatch()

  // useEffect(() => {

  //   dispatch(fetchCurrentUser())

  // }, [dispatch])

  return (
    <React.Fragment>

      <AppHeader />

      <AppWithRouting />

    </React.Fragment>
  )

}

export default withRouter(App)
