// Libs
import React from 'react'
import { Route, Switch } from 'react-router-dom'

// Pages
import HomePage from './pages/HomePages'
import LoginPage from './pages/LoginPage'
import RegisterPage from './pages/RegisterPage'
import ProductsPage from './pages/ProductsPage'
import ProductsCreatePage from './pages/ProductsCreatePage'
import ProductsDetail from './pages/ProductsDetail'
import ProductEditPage from './pages/ProductsEditPage'

// Components
import HandleAuthenRoute from './components/HandleAuthenRoute'

export default () => (
    <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/register' component={RegisterPage} />

        <Route exact path='/products/' component={HandleAuthenRoute(ProductsPage)} />
        <Route exact path='/products/create' component={HandleAuthenRoute(ProductsCreatePage)} />
        <Route exact path='/products/:id' component={ProductsDetail} />
        <Route exact path='/products/edit/:id' component={HandleAuthenRoute(ProductEditPage)} />

        {/* <React exact={true} path='*' /> */}
    </Switch>
)