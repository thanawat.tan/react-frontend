// Libs
import { combineReducers } from 'redux'
import { reducer as fromReducer } from 'redux-form'
import { connectRouter } from 'connected-react-router'

// reducers
import productsReducer from './productsReducer'
import authenReducer from './authenReducer'

const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    form: fromReducer,
    products: productsReducer,
    authen: authenReducer
})

export default createRootReducer