//// Utils
import { createActionSet } from '../../utilities'

const USER_LOGIN = createActionSet('USER_LOGIN')
const USER_REGISTER = createActionSet('USER_REGISTER')
const USER_LOGOUT = createActionSet('USER_LOGOUT')
const FETCH_CURRENT_USER = createActionSet('FETCH_CURRENT_USER')

export const initialState = {
    isFetching: false,
    error: null,
    currentUser: {},
    isAuthenticated: false
}

export default (state = initialState, dispatch) => {

    const { type, payload, error } = dispatch

    switch (type) {

        // Pending
        case FETCH_CURRENT_USER.PENDING:
        case USER_LOGIN.PENDING:
        case USER_REGISTER.PENDING:
        case USER_LOGOUT.PENDING:
            return {
                ...state,
                isFetching: true
            }

        // Success
        case FETCH_CURRENT_USER.SUCCESS:
        case USER_LOGIN.SUCCESS:
        case USER_REGISTER.SUCCESS:
            return {
                ...state,
                isFetching: false,
                currentUser: payload,
                isAuthenticated: true
            }

        // Failed
        case FETCH_CURRENT_USER.FAILED:
        case USER_LOGIN.FAILED:
        case USER_REGISTER.FAILED:
        case USER_LOGOUT.SUCCESS:
            return {
                ...state,
                isFetching: false,
                error,
                currentUser: {},
                isAuthenticated: false
            }

        default:
            return state
    }
}