// Libs
import React, { useState } from 'react'

// Material UI
import { Snackbar } from '@material-ui/core/'
import { makeStyles } from '@material-ui/core/styles'
import MuiAlert from '@material-ui/lab/Alert'

// Set CSS
const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        }
    }
}))

const Alert = props => {
    return <MuiAlert elevation={6} variant="filled" {...props} />
}

const LoginErrorMessage = props => {

    const { message } = props

    const classes = useStyles()

    const [open, setOpen] = useState(true)

    const handleClose = (e, reason) => {

        if (reason === 'clickaway') {
            return
        }

        setOpen(false)
    }

    return (
        <div className={classes.root}>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert severity='error' onClose={handleClose}>{message}</Alert>
            </Snackbar>
        </div>
    )

}

export default LoginErrorMessage