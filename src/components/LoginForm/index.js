// Libs 
import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Field, reduxForm } from 'redux-form'

// Components 
import LoginErrorMessage from './LoginErrorMessage'

// Material UI
import {
    Container,
    Paper,
    CssBaseline,
    TextField,
    Typography,
    Grid,
    Button
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// Set CSS
const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}))


const renderTextField = ({ label, input, meta: { touched, invalid, error }, ...custom }) => (
    <TextField
        label={label}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
    />
)

const LoginForm = props => {

    const { title, isLoginForm, handleSubmit } = props

    const classes = useStyles()

    const error = useSelector(state => state.authen.error)

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5">{title}</Typography>
                <form className={classes.form} noValidate onSubmit={handleSubmit} >

                    <Field
                        variant='outlined'
                        margin='normal'
                        required
                        fullWidth
                        id='email'
                        label='Email Address'
                        name='email'
                        autoComplete='email'
                        autoFocus
                        component={renderTextField}
                    />

                    <Field
                        variant='outlined'
                        margin='normal'
                        required
                        fullWidth
                        name='password'
                        label='Password'
                        type='password'
                        id='password'
                        autoComplete='current-password'
                        component={renderTextField}
                    />

                    <Button
                        type='submit'
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                    >
                        LOGIN
                    </Button>
                    <Grid container>
                        {
                            isLoginForm ?
                                (
                                    <Grid item>
                                        <Link to='/register' >
                                            Don't have an account? Register
                                    </Link>
                                    </Grid>

                                ) :
                                (
                                    <Grid item>
                                        <Link to='/login' >
                                            Back to login
                                        </Link>
                                    </Grid>
                                )
                        }
                    </Grid>
                    {
                        error ? <LoginErrorMessage message={error.message} /> : null
                    }
                </form>
            </Paper>
        </Container >
    )
}

export default reduxForm({ form: 'authen-form' })(LoginForm)
