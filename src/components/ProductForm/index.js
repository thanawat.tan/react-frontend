// Libs
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { reduxForm, Field } from 'redux-form'

// Components
import DropzoneUploader from './DropzoneUploader'

// Material UI
import {
    Container,
    CssBaseline,
    Typography,
    TextField,
    Button,
    Paper,
    Grid
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submitButton: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    cancelButton: {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    grid: {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(0.5),
        marginBottom: theme.spacing(1)
    },
    title: {
        marginTop: theme.spacing(0.5)
    }
}))

const renderTextField = ({ label, input, meta: { touched, invalid, error }, ...custom }) => (
    <TextField
        label={label}
        placeholder={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
    />
)

const ProductFrom = props => {

    const [imageUrl, setImageUrl] = useState('')

    const { title, handleSubmit } = props

    const classes = useStyles()

    useEffect(() => {

        setImageUrl(props.imageUrl)

    }, [props.imageUrl])

    const handleUploadFile = value => {

        setImageUrl(value.imageUrl)

    }

    return (
        <Container component='main' maxWidth='md'>
            <CssBaseline />
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5">{title}</Typography>
                <form className={classes.form} noValidate onSubmit={handleSubmit} >

                    {/* Product Name */}
                    <Field
                        variant='outlined'
                        margin='normal'
                        fullWidth
                        id='products_name'
                        label='Products Name'
                        name='products_name'
                        autoFocus
                        component={renderTextField}
                    />

                    {/* Product Image */}
                    {imageUrl ? (
                        <div>
                            <img src={imageUrl} alt='Preview Product' width='864' height='520' />
                        </div>
                    ) : null}

                    <Grid container className={classes.grid}>
                        <Typography variant='h6' className={classes.title}>
                            Product Image :
                            </Typography>

                        <Field
                            name='products_image'
                            component={DropzoneUploader}
                            onChange={handleUploadFile}
                            handleProgress={progress => {
                                console.log('progress', progress)
                            }}
                        />
                    </Grid>

                    {/* Product Price */}
                    <Field
                        variant='outlined'
                        margin='normal'
                        fullWidth
                        id='products_price'
                        label='Price'
                        name='products_price'
                        autoFocus
                        component={renderTextField}
                    />

                    {/* Product Quantity */}
                    <Field
                        variant='outlined'
                        margin='normal'
                        fullWidth
                        id='products_quantity'
                        label='Quantity'
                        name='products_quantity'
                        autoFocus
                        component={renderTextField}
                    />

                    {/* Product Category */}
                    <Field
                        variant='outlined'
                        margin='normal'
                        fullWidth
                        id='products_category'
                        label='Category'
                        name='products_category'
                        autoFocus
                        component={renderTextField}
                    />

                    {/* Product Description */}
                    <Field
                        variant='outlined'
                        margin='normal'
                        fullWidth
                        id='products_description'
                        label='Description'
                        name='products_description'
                        autoFocus
                        component={renderTextField}
                    />

                    {/* Submit Button */}
                    <Button
                        type='submit'
                        variant='contained'
                        color='primary'
                        className={classes.submitButton}
                    >
                        SUBMIT
                    </Button>

                    {/* Cancel Button */}
                    <Button
                        type='cancel'
                        variant='outlined'
                        className={classes.cancelButton}
                    >
                        <Link to='/products' style={{ textDecoration: 'none', color: '#000000' }}>
                            CANCEL
                        </Link>
                    </Button>
                </form>
            </Paper>
        </Container >
    )
}

export default reduxForm({
    form: 'product-from',
    enableReinitialize: true
})(ProductFrom)