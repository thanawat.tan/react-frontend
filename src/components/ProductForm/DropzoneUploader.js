// Utils
import { storage } from '../../utilities/firebase'

// Libs
import React, { useCallback } from 'react'
import { useDropzone } from 'react-dropzone'

// Material UI 
import { Button } from '@material-ui/core'
import { CloudUpload } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'

// Use Func Storafe Ref
const storageRef = storage.ref()

// Set CSS
const useStyles = makeStyles(theme => ({
    button: {
        marginLeft: theme.spacing(2)
    }
}))

const DropzoneUploader = props => {

    const classes = useStyles()

    const onDrop = useCallback(acceptedFiles => {

        const file = acceptedFiles[0]

        const uploadTask = storageRef.child(`/products/${file.name}`).put(file)

        uploadTask.on('state_changed',
            snapshot => {
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                props.handleProgress(progress)
            },
            (error) => {
                console.log('error', error)
            },
            () => {
                // success
                uploadTask.snapshot.ref.getDownloadURL().then(url => {
                    props.input.onChange({
                        imageUrl: url
                    })
                })
            }
        )

    }, [props])

    const { getRootProps, getInputProps } = useDropzone({ onDrop })

    return (
        <div {...getRootProps()}>
            <input {...getInputProps()} />
            <Button
                variant='contained'
                color='default'
                className={classes.button}
                startIcon={<CloudUpload />}
            >
                UPLOAD FILE
            </Button>
        </div>
    )

}

export default DropzoneUploader
